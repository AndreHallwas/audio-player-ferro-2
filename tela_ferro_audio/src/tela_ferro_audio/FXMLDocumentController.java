/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tela_ferro_audio;
import Classes.Audio;
import Classes.Desenha;
import Classes.DesenhaL;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.paint.Color;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JOptionPane;

/**
 *
 * @author Luish
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Canvas canvas;
    @FXML
    private Label nome_musica;
    private GraphicsContext gc;
    @FXML
    private ComboBox<String> menu;
    @FXML
    private Button btnopen;
    @FXML
    private Button btnplay;
    @FXML
    private Button btnpause;
    @FXML
    private Button btnstop;
    private Audio som;
    @FXML
    private ProgressBar barraprogresso;
    @FXML
    private Canvas canvasL;
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        ObservableList<String> options = FXCollections.observableArrayList( "Inverter", "Reduzir Duração", "Aumentar Som" , "Aumentar Duração");
        menu.setItems(options);
        desabilitar_buttons();
        som = new Audio();
        gc = canvas.getGraphicsContext2D();
    }
    public void desabilitar_buttons()
    {
        menu.setDisable(true);
        btnplay.setDisable(true);
        btnpause.setDisable(true);
        btnstop.setDisable(true);
    }
    public void abilitar_buttons()
    {
        menu.setDisable(false);
        btnplay.setDisable(false);
        btnpause.setDisable(false);
        btnstop.setDisable(false);
    }
    public void atualiza_canvas()
    {
        Desenha d = new Desenha(canvas,1);
        for(int k=0;k < som.getAudiostream().length;k+= som.getAudiostream().length*0.0004)
        {
            d.desenhar((double)som.getAudiostream()[k]);
        }
        canvas = d.getCanvas();
        gc.stroke();
    }
    public void aumetar_som() throws LineUnavailableException
    {
        som.aumetar_som();
    }
    @FXML
    private void evtOpen(ActionEvent event)
    {
        try
        {
            som.inicializa();
            atualiza_canvas();
            abilitar_buttons();
            ativarThread();
            ativarThread2();
            nome_musica.setText(som.getNome_musica());
        }
        catch(Exception e)
        {
            desabilitar_buttons();
            JOptionPane.showMessageDialog(null, "Erro ao Abrir o Arquivo");
        }
    }
    @FXML 
    @SuppressWarnings("empty-statement")
    private void evtPlay(ActionEvent event) throws LineUnavailableException, InterruptedException 
    {
        som.Play();
    }

    @FXML 
    private void evtPause(ActionEvent event) 
    {
        som.Pause();
    }

    @FXML
    private void evtStop(ActionEvent event) 
    {
        som.Stop();
        som.voltar_musica_inicio(0);
        barraprogresso.setProgress(0);
    }
    @FXML
    private void evtMenu(ActionEvent event) throws LineUnavailableException 
    {
        /////System.out.println(menu.getItems().get(menu.getSelectionModel().getSelectedIndex()));
        //System.out.println(menu.getSelectionModel().getSelectedItem());
        switch(menu.getSelectionModel().getSelectedItem())
        {
            case "Inverter":
                som.inverter();
            break;
            case "Reduzir Duração":
                som.reduzir_duração();
            break;
            
            case "Aumentar Som":
                som.aumetar_som();
            break;
            case "Aumentar Duração":
                som.aumentar_duração();
            break;
        }
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        atualiza_canvas();
        /*
        barraprogresso.setProgress(0);
        barraprogresso.getProgress();
        */
    }
     private void ativarThread()
    {
       new Timer().schedule(new TimerTask() 
       {
           @Override
           public void run()
           {
               Platform.runLater(()->{processo();});
           }
       }, 0 ,100 );
    }
     
     private void processo()
     {
        if(som.getAudio().isRunning())
        {
           barraprogresso.setProgress(0);
           barraprogresso.setProgress((double)(som.getAudio().getFramePosition()*1)/(som.getStream().getFrameLength()));
          System.out.println((double)(som.getAudio().getFramePosition()*1)/som.getStream().getFrameLength());
        }  
     }
     
     private void ativarThread2()
    {
       new Timer().schedule(new TimerTask() 
       {
           @Override
           public void run()
           {
               Platform.runLater(()->{playBack();});
           }
       }, 0 ,100 );
    }
     
     public void playBack(){
        DesenhaL d = new DesenhaL(canvasL,1);
        d.setConstante(8);
        d.mostraLinha(som, d);
     }
    
}
