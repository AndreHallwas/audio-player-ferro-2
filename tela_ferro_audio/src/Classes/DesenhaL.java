/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import static Classes.Desenha.gc;
import javafx.scene.canvas.Canvas;
import javafx.scene.paint.Color;

/**
 *
 * @author andre
 */
public class DesenhaL extends Desenha {
    
    public DesenhaL(Canvas c, double Divisao) {
        super(c, Divisao);
        gc.setStroke(Color.WHITE);
    }
    @Override
    public void desenhar(double Amostra)
    {
        gc.strokeLine(antX,255/Constante,Count+2,(Amostra+127)/Constante);
        incrementaCount(Amostra);
    } 
    public void mostraLinha(Audio som ,DesenhaL d){
        limpaCanvas();
        for(int k=som.getAudio().getFramePosition()+1;k <= som.getAudio().getFramePosition()+10;k++)
        {
            d.desenhar((double)som.getAudiostream()[k]);
        }
        canvas = d.getCanvas();
        gc.stroke();
     }
    @Override
    protected void incrementaCount(double Amostra)
    {
        canvas.setWidth(canvas.getWidth()+9);
        Count+=9;
        antX = Count;
        antY = (Amostra+127)/Constante;
    }
    
}
