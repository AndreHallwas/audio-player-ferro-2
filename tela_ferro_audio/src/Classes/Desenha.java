/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author Luish
 */
public class Desenha 
{
    protected static double Count;
    protected static GraphicsContext gc;
    protected static Canvas canvas;
    protected static double Constante;
    protected static double antX;
    protected static double antY;
    
    public Desenha(Canvas c,double Divisao) 
    {
        Count = 0;
        canvas = c;
        canvas.setWidth(1);
        gc = c.getGraphicsContext2D();
        gc.setFill(Color.BLUE);
        gc.setStroke(Color.BLACK);
        setConstante(Divisao);
        antX = antY = 0;
        limpaCanvas();
    }
    
    public void desenhar(double Amostra)
    {
        gc.lineTo(Count,(Amostra+127)/Constante);
        incrementaCount(Amostra);
    }  

    public void setConstante(double Constante) {
        if(Constante > 0)
            Desenha.Constante = Constante; 
        else
            Desenha.Constante = 1;
    }
     protected void incrementaCount(double Amostra)
    {
        canvas.setWidth(canvas.getWidth()+1);
        Count++;
    }

    public Canvas getCanvas() 
    {
        return canvas;
    }
    
    public void limpaCanvas(){
        gc.clearRect(0, 0, canvas.getWidth(),canvas.getHeight());
        canvas.setWidth(1);
        gc.moveTo(0, 0);
    }
}
