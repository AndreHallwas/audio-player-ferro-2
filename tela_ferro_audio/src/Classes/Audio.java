/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.io.File;
import java.io.IOException;
import javafx.stage.FileChooser;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author Aluno
 */
public class Audio 
{
    private AudioInputStream stream;  // sequência de bytes do wave
    private AudioFormat format;       // configuração de formato
    private DataLine.Info info;       // informações sobre o formato
    private Clip audio;
    private byte[] audiostream;
    private String nome_musica;
    private File arq;
   public Audio(){
       
   }
   public void inicializa() throws Exception
   {
       FileChooser endereco = new FileChooser();
        arq = endereco.showOpenDialog(null);
        nome_musica = (arq.getName());
        stream = AudioSystem.getAudioInputStream(new File(arq.toURI()));
        format = stream.getFormat();
        info = new DataLine.Info(Clip.class, format);
        audio = (Clip) AudioSystem.getLine(info);
        /////audiostream = new byte[(int)stream.getFrameLength()];
        audiostream = new byte[(int)arq.length()];
        stream.read(audiostream);
        audio = (Clip) AudioSystem.getLine(info);
        /////audio.open(format, audiostream, 0, (int)stream.getFrameLength());
        audio.open(format, audiostream, 0, (int)arq.length());
   }
   public void aumetar_som() throws LineUnavailableException/////TROCAR OS STREAM.FRAMELENGHT() POR arq.length();
    {
        for (int i = 0; i < audiostream.length; i++)
        {
            audiostream[i] = (byte)((byte)audiostream[i]*1.6);
        }
        audio = (Clip) AudioSystem.getLine(info);
        audio.open(format, audiostream, 0, (int)stream.getFrameLength());
    }
   public void reduzir_duração() throws LineUnavailableException
    {
        byte aux[];
        aux = new byte[audiostream.length/2];
        for (int i = 0, k = 0; i < audiostream.length/2; i+=2, k++) 
        {
                aux[k] = audiostream[i];
        }
        audio = (Clip) AudioSystem.getLine(info);
        audio.open(format, aux, 0, (int)stream.getFrameLength()/2);////Verificar
    }
   public void aumentar_duração() throws LineUnavailableException
    {
        byte aux[];
        aux = new byte[audiostream.length*2];
        for (int i = 0, k = 0; k < audiostream.length; i+=2, k++) 
        {
                aux[i] = audiostream[k];
                aux[i+1] = audiostream[k];
        }
        audio = (Clip) AudioSystem.getLine(info);
        audio.open(format, aux, 0, (int)stream.getFrameLength()*2);
    }
   public void inverter() throws LineUnavailableException
    {
        byte aux;
        for(int i=0, j=audiostream.length-1;i<audiostream.length/2;i++,j--)//inverte a musica
        {   aux=audiostream[i];
            audiostream[i]=audiostream[j];
            audiostream[j]=aux;            
        }
        audio = (Clip) AudioSystem.getLine(info);
        audio.open(format, audiostream, 0, (int)stream.getFrameLength());
    }
   public void Play() throws LineUnavailableException, InterruptedException 
    {
        audio.start();
    }
   public void Pause() 
    {
        audio.stop();
    }
   public void Stop() 
    {
        audio.stop();
        audio.setFramePosition(0);
    }
    public Audio(AudioInputStream stream, AudioFormat format, DataLine.Info info, Clip audio, byte[] audiostream) {
        this.stream = stream;
        this.format = format;
        this.info = info;
        this.audio = audio;
        this.audiostream = audiostream;
    }

    public AudioInputStream getStream() {
        return stream;
    }

    public AudioFormat getFormat() {
        return format;
    }

    public DataLine.Info getInfo() {
        return info;
    }

    public Clip getAudio() {
        return audio;
    }

    public byte[] getAudiostream() {
        return audiostream;
    }

    public void setStream(AudioInputStream stream) {
        this.stream = stream;
    }

    public void setFormat(AudioFormat format) {
        this.format = format;
    }

    public void setInfo(DataLine.Info info) {
        this.info = info;
    }

    public void setAudio(Clip audio) {
        this.audio = audio;
    }

    public void setAudiostream(byte[] audiostream) {
        this.audiostream = audiostream;
    }
    public void voltar_musica_inicio(int i)
    {
        audio.setFramePosition(i);
    }
    public void abrir_musica(AudioFormat format, byte[] data, int offset, int bufferSize) throws LineUnavailableException
    {
        audio.open(format, data, offset, bufferSize);
    }

    public String getNome_musica() {
        return nome_musica;
    }
    
}
